plugins=(git)

# vim keybindings
bindkey -v
export ZSHDIR="$HOME/.zsh"

# OS-dependent configuration
case "$OSTYPE" in
  linux*)
    source $ZSHDIR/.zsh_linux
  ;;
  darwin*)
    source $ZSHDIR/.zsh_mac
  ;;
esac

# source work aliases
source $ZSHDIR/.zsh_work

export REPOS="$HOME/Projects"
export EDITOR="vim"
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
alias sudo='sudo '
alias ls='ls --color=auto'
alias ll='ls -la --color=auto'
alias l.='ls -d .* --color=auto'

# fzf aliases
alias peek='fzf --preview "bat --style=numbers --color=always {}"'

# ZSH fpath for Pure
fpath=("$ZSHDIR" $fpath)

#Add Ruby gems to Path
#PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"
PATH="$HOME/.local/bin:$PATH"

# autojump
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

# auto completion
autoload -Uz compinit
compinit

# pure prompt
autoload -U promptinit; promptinit
prompt pure

# syntax highlighting
source $ZSHDIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSHDIR/zsh-autosuggestions/zsh-autosuggestions.zsh

# fuzzy finder
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
