call plug#begin('~/.vim/plugged')
" Plugins {{{
Plug 'vim-airline/vim-airline'
Plug 'edkolev/tmuxline.vim'
Plug 'morhetz/gruvbox'
Plug 'Yggdroot/indentLine'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-surround'
Plug 'pangloss/vim-javascript'
Plug 'airblade/vim-gitgutter'
Plug 'mattn/emmet-vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all'}
Plug 'junegunn/fzf.vim'
Plug 'tmhedberg/SimpylFold'
Plug 'vim-scripts/indentpython.vim'
Plug 'pearofducks/ansible-vim'
Plug 'vimwiki/vimwiki'
Plug 'w0rp/ale'
Plug 'christoomey/vim-tmux-navigator'
" keep this last in plugin list
Plug 'ryanoasis/vim-devicons'


call plug#end()
filetype plugin on
set hidden
set updatetime=250
" }}}
" Editing {{{
syntax on
set termguicolors
set tabstop=4 " visual spaces per TAB
set softtabstop=4 " spaces inserted for each TAB
set shiftwidth=4
set expandtab " makes TAB turn into spaces
set autoindent
set backspace=indent,eol,start
set clipboard^=unnamed,unnamedplus
let g:ale_fixers = {
            \'*': ['remove_trailing_lines', 'trim_whitespace'],
            \'php': ['prettier', 'php_cs_fixer'],
            \'css': ['prettier'],
            \'javascript': ['prettier', 'eslint'],
            \'python': ['yapf']
            \}
let g:ale_fix_on_save = 1
set autochdir "auto change directory to buffer

" VimWiki
let g:vimwiki_list = [{'path': '~/Journal/', 'syntax': 'markdown', 'ext': '.md'}]
" }}}
" UI {{{
set visualbell
set confirm
set number " line numbers
set showcmd " show previous command
set cursorline " highlight current line
filetype indent plugin on " activate filetype detection
set showmatch " highlight matching parentheses
set lazyredraw " faster macros
set wildmenu " autocomplete for commands
set guicursor=

" Word wrapping
:set wrap linebreak nolist
call matchadd('ColorColumn', '\%81v', 100) "highlight past 80th column

" Git Gutter symbols
let g:gitgutter_sign_added='┃'
let g:gitgutter_sign_modified='┃'
let g:gitgutter_sign_removed='◢'
let g:gitgutter_sign_removed_first_line='◥'
let g:gitgutter_sign_modified_removed='◢'

" ALE Symbols
let g:ale_sign_error = 'X'
let g:ale_sign_warning = '=>'

" Goyo Limelight integration
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" Whitespace characters
set list
set listchars=
set listchars+=tab:░\
set listchars+=eol:¬
set listchars+=trail:·
set listchars+=extends:»
set listchars+=precedes:«
set listchars+=nbsp:⣿

" Indent line
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_leadingSpaceChar = '·'

" netrw settings
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
let g:netrw_banner = 0

" fzf hide terminal statusline
autocmd! FileType fzf
autocmd FileType fzf set laststatus=0 noshowmode noruler
    \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler

" }}}
" Search {{{
set incsearch " search while typing
set hlsearch " highlight search results
set ignorecase
set smartcase
" }}}
" Folding {{{
let g:SimpylFold_docstring_preview=1 " SimplyFold doc string preview
set foldenable " enable folding
set foldmethod=indent
set foldlevel=99 " open most folds by default
" enable folding with spacebar
nnoremap <space> za
" }}}
" Movement {{{
    " move vertically by visual line
nnoremap j gj
nnoremap k gk
    " highlight last inserted text
nnoremap gV `[v`]
set mouse=a " mouse movement

" Navigate windows with ctrl
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set whichwrap+=<,>,h,l,[,]

" Splitting
set splitbelow
set splitright

" Use esc in terminal buffers
tnoremap <Esc> <C-\><C-n>

" }}}
" Backup {{{
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup
" }}}
" Airline {{{
let g:gruvbox_bold=1
let g:gruvbox_underline=1
let g:gruvbox_undercurl=1
let g:gruvbox_italic=1
let g:gruvbox_italicize_comments=1
let g:gruvbox_italicize_strings=1
let g:gruvbox_improved_warnings=1
colorscheme gruvbox
set background=dark

let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
"let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1 " enable airline support for ALE
" }}}

" vim:foldmethod=marker:foldlevel=0
